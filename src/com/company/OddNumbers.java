package com.company;

public class OddNumbers {

    // цикл for
    public static void printNumber1() {
        for (int i = 1; i <= 100; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    // цикл while
    public static void printNumber2() {
        int i = 1;
        while (i < 100) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
            i++;
        }
        System.out.println();
    }

    // цикл do - while
    public static void printNumber3() {
        int i = 1;
        do {
            if (i % 2 != 0)
                System.out.print(i + " ");
            i++;
        }
        while (i < 100);
        System.out.println();
    }

}

