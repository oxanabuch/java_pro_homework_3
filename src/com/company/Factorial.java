package com.company;

import java.util.Scanner;

public class Factorial {

    // цикл for
    public static void  calculateFactorial1() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введіть число N: ");
        int n = in.nextInt();

        int result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        System.out.println(result);
        System.out.println();
    }

    // цикл while
    public static void calculateFactorial2() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введіть число N: ");
        int n = in.nextInt();

        int result = 1;
        int i = 1;
        while (i <= n) {
            result *= i;
            i++;
        }
        System.out.println(result);
        System.out.println();
    }

    // цикл do - while
    public static void calculateFactorial3() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введіть число N: ");
        int n = in.nextInt();

        int result = 1;
        int i = 1;
        do {
            result *= i;
            i++;
        }
        while (i <= n);
        System.out.println(result);
        System.out.println();
    }

}

