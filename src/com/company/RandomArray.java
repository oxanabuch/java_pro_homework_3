package com.company;

import java.util.Arrays;
import java.util.OptionalInt;
import java.util.Scanner;

public class RandomArray {

    public static void minMaxElement() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введіть розмірність масиву N: ");
        int n = in.nextInt();
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        int[] randomArr = new int[n];

        for (int i = 0; i < randomArr.length; i++) {
            randomArr[i] = (int) (Math.random() * 101);
        }
        System.out.print(Arrays.toString(randomArr));
        System.out.println();

        for (int i = 0; i < randomArr.length; i++) {
            min = Math.min(min, randomArr[i]);
            max = Math.max(max, randomArr[i]);
        }
        System.out.println("The smallest element of array is: " + min);
        System.out.println("The largest element of array is: " + max);
        System.out.println();
    }
}


