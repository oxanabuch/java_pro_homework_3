package com.company;

public class Main {

    public static void main(String[] args) {

        // Завдання 1
        System.out.println("1. За допомогою циклу for вивести на екран непарні числа від 1 до 99");
        OddNumbers.printNumber1();
        System.out.println();
        System.out.println("-------");

        // Завдання 2
        System.out.println("2. Дано число n. За допомогою циклу for порахувати факторіал n!");
        Factorial.calculateFactorial1();
        System.out.println("-------");

        // Завдання 3
        System.out.println("3.1. За допомогою циклу while вивести на екран непарні числа від 1 до 99");
        OddNumbers.printNumber2();
        System.out.println();
        System.out.println("-------");


        System.out.println("3.2. Дано число n. За допомогою циклу while  порахувати факторіал n!");
        Factorial.calculateFactorial2();
        System.out.println("-------");

        // Завдання 4
        System.out.println("4.1. За допомогою циклу do - while вивести на екран непарні числа від 1 до 99");
        OddNumbers.printNumber3();
        System.out.println();
        System.out.println("-------");

        System.out.println("4.2. Дано число n. За допомогою циклу do - while  порахувати факторіал n!");
        Factorial.calculateFactorial3();
        System.out.println("-------");

        // Завдання 5
        System.out.println("5. Дано змінні x та n, обчислити x^n");
        RaisingNumber.raisingToPower();
        System.out.println("-------");

        // Завдання 6
        System.out.println("6. Вивести 10 перших чисел послідовності 0, -5, -10, -15 ...");
        Sequence.sequenceOfFive();
        System.out.println();
        System.out.println("-------");

        // Завдання 7
        System.out.println("7. Потрібно вивести на екран таблицю множення на x (x - будь-яке число в діапазоні 0 ... 10)");
        MultiplicationTable.multiplication();
        System.out.println();
        System.out.println("-------");

        // Завдання 8
        System.out.println("8. Створіть масив, що містить 10 перших непарних чисел.");
        System.out.println("Виведіть елементи масиву на консоль в один рядок, розділюючи комою");
        OddArray.printArrayTen();
        System.out.println();
        System.out.println("-------");

        // Завдання 9 + Завдання 10
        System.out.println("9. Дано масив розмірності N. Знайти найменший елемент масиву та вивести на консоль");
        System.out.println("Масив заповнити випадковими числами з діапазону 0...100");
        System.out.println("10. В масиві із завдання 9 знайти найбільший елемент: ");
        RandomArray.minMaxElement();
        System.out.println("-------");

        // Завдання 11
        System.out.println("11. Замінити місцями найбільший та найменший елементи масиву місцями.");
        System.out.println("Приклад: дано масив {4, -5, 0, 6, 8}. Після заміни буде: {4, 8, 0, 6, -5}");
        SwapElements.swapMinMaxElement();
        System.out.println();
        System.out.println("-------");

        // Завдання 12
        System.out.println("12. Вивести на екран шахову дошку 8*8 у вигляді 2-мірного масиву.");
        System.out.println("(W - білі клітинки, В - чорні клітинки)");
        ChessBoard.printChessBoard();
        System.out.println();
        System.out.println("-------");

    }
}
