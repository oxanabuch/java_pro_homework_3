package com.company;

import java.util.Arrays;

public class OddArray {

    public static void printArrayTen () {

        int[] tenArray = new int[10];
        for (int i = 0; i < 10; i++) {
            tenArray[i] = 2 * i + 1;
        }
        System.out.print(Arrays.toString(tenArray));
        System.out.println();
    }
}
