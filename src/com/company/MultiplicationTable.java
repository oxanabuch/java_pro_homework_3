package com.company;

import java.util.Scanner;

public class MultiplicationTable {

    public static void multiplication() {

        Scanner in = new Scanner(System.in);
        System.out.println("Input a number: ");
        int x = in.nextInt();

        for (int i = 1; i <= 10; i++) {
            int result;
            result = x * i;
            System.out.println( x + " * " + i + " = " + result);
        }
    }

}
