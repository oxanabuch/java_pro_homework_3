package com.company;

import java.util.Arrays;
import java.util.OptionalInt;

public class SwapElements {

    public static void swapMinMaxElement() {
        int[] digits = {7, -25, -199, 8, 9, 15, 3, 358, 0};

        OptionalInt min = Arrays.stream(digits).min();
        OptionalInt max = Arrays.stream(digits).max();
        int minPos = -1;
        int maxPos = -1;

        System.out.println("Initial array: " + Arrays.toString(digits));
        for (int i = 0; i < digits.length; i++) {

            if (digits[i] == min.getAsInt()) {
                minPos = i;
            }

            if (digits[i] == max.getAsInt()) {
                maxPos = i;
            }

            if (minPos != -1 && maxPos != -1) {
                digits[minPos] = max.getAsInt();
                digits[maxPos] = min.getAsInt();
            }
        }
        System.out.println("Changed array: " + Arrays.toString(digits));
    }
}

